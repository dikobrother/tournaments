using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class MatchInfo : MonoBehaviour
{
    [SerializeField] private TMP_Text _firstTeamName;
    [SerializeField] private TMP_Text _secondTeamName;
    [SerializeField] private Image _firstLogo;
    [SerializeField] private Image _secondLogo;
    [SerializeField] private TMP_Text _statusText;
    [SerializeField] private TMP_Text _timeText;
    [SerializeField] private TMP_Text _firstScore;
    [SerializeField] private TMP_Text _secondScore;

    public void Init(string firstName, string secondName, ImageLoader imageLoader, string firstLogo, string secondLogo, int time, string status, int firstScore, int secondScore)
    {
        _firstTeamName.text = firstName;
        _secondTeamName.text = secondName;
        imageLoader.AddImageToQueue(firstLogo, _firstLogo);
        imageLoader.AddImageToQueue(secondLogo, _secondLogo);
        _timeText.text = "������ �����: " + DateTimeOffset.FromUnixTimeSeconds((int)time).DateTime.ToShortDateString() + " " + DateTimeOffset.FromUnixTimeSeconds((int)time).DateTime.ToShortTimeString();
        _statusText.text = status;
        _firstScore.text = firstScore.ToString();
        _secondScore.text = secondScore.ToString();
    }
}
