using OneDevApp.CustomTabPlugin;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoadingCanvas : MonoBehaviour
{
    [SerializeField] private GameObject _loadingCanvas;
    [SerializeField] private GameObject _loading;
    [SerializeField] private Button _loadButton;

    private void Awake()
    {
        var reg = SaveSystem.LoadData<RegistrationSaveData>();
        if (reg.Confirmed)
        {
            _loading.SetActive(true);
        }
        else
        {
            _loading.SetActive(false);
        }
        _loadButton.onClick.AddListener(OnLoadButtonClicked);
    }

    public void OnLoadButtonClicked()
    {
        var reg = SaveSystem.LoadData<RegistrationSaveData>();
#if UNITY_ANDROID && !UNITY_EDITOR
                ChromeCustomTab.OpenCustomTab(reg.Link, "#000000", "#000000", false, false);
#endif
    }

    public void OpenMenu()
    {
        _loadingCanvas.SetActive(false);
    }

    public void OpenLoading()
    {
        _loadingCanvas.SetActive(true);
    }
}
