using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using static TMPro.TMP_Dropdown;

public class TournamentShower : MonoBehaviour
{
    [SerializeField] private GameObject _menuCanvas;
    [SerializeField] private GameObject _tournamentsCanvas;
    [SerializeField] private List<OptionData> _gameNames;
    [SerializeField] private TMP_Dropdown _dropNames;
    [SerializeField] private TMP_Dropdown _dropSeasons;
    [SerializeField] private GetFootballStandings _footballStandings;
    [SerializeField] private GetHandballStandings _handballStandings;
    [SerializeField] private GetVolleyballStandings _volleyballStandings;
    [SerializeField] private GetHockeyStandings _hockeyStandings;
    [SerializeField] private GetBaseballStandings _baseballStandings;

    private void Start()
    {
        var games = SaveSystem.LoadData<GamesSaveData>();
        List<OptionData> names = new List<OptionData>();
        for (int i = 0; i < games.Games.Count; i++)
        {
            if (games.Games[i] == true)
            {
                names.Add(_gameNames[i]);
                LoadTournaments(i);
            }
            Debug.Log(i);
        }
        _gameNames.Clear();
        _gameNames = names;
        _dropNames.ClearOptions();
        _dropNames.AddOptions(_gameNames);
        _dropNames.RefreshShownValue();
    }

    public void UpdateTournaments()
    {
        string currentType = _dropNames.options[_dropNames.value].text;
        string currentSeason = _dropSeasons.options[_dropSeasons.value].text;
        _footballStandings.HideAllMatches();
        _handballStandings.HideAllMatches();
        _volleyballStandings.HideAllMatches();
        _hockeyStandings.HideAllMatches();
        _baseballStandings.HideAllMatches();
        switch (currentType)
        {
            case "������":
                {
                    _footballStandings.ShowMatchesBySeason(int.Parse(currentSeason));
                    break;
                }
            case "�������":
                {
                    _handballStandings.ShowMatchesBySeason(int.Parse(currentSeason));
                    break;
                }
            case "��������":
                {
                    _volleyballStandings.ShowMatchesBySeason(int.Parse(currentSeason));
                    break;
                }
            case "������":
                {
                    _hockeyStandings.ShowMatchesBySeason(int.Parse(currentSeason));
                    break;
                }
            case "�������":
                {
                    _baseballStandings.ShowMatchesBySeason(int.Parse(currentSeason));
                    break;
                }
        }

    }

    public void OpenGames()
    {
        _menuCanvas.SetActive(false);
        _tournamentsCanvas.SetActive(true);
        UpdateTournaments();
    }

    public void CloseGames()
    {
        _menuCanvas.SetActive(true);
        _tournamentsCanvas.SetActive(false);
    }

    public void LoadTournaments(int index)
    {
        switch (index)
        {
            case 0:
                {
                    _footballStandings.LoadFootballLeagues();
                    break;
                }
            case 1:
                {
                    _handballStandings.LoadHandballLeagues();
                    break;
                }
            case 2:
                {
                    _volleyballStandings.LoadVolleyballLeagues();
                    break;
                }
            case 3:
                {
                    _hockeyStandings.LoadVolleyballLeagues();
                    break;
                }
            case 4:
                {
                    _baseballStandings.LoadVolleyballLeagues();
                    break;
                }
        }
    }
}
