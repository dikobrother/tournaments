using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class GetHockeyMatches : MonoBehaviour
{
    [SerializeField] private ImageLoader _imageLoader;
    [SerializeField] private Transform _containerOne;
    [SerializeField] private Transform _containerTwo;
    [SerializeField] private Transform _containerThree;
    [SerializeField] private Transform _containerFour;
    [SerializeField] private Transform _containerFive;
    [SerializeField] private MatchInfo _matchInfo;
    [SerializeField] private List<GameObject> _noMatchError;
    private readonly string _apiKey = "c599039951fde9d17080eb48dd713268"; // �������� �� ��� ���� API
    private readonly string _apiUrl = "https://v1.hockey.api-sports.io/games";
    private string _todayUrl;
    private string _yesterdayUrl;
    private string _tomorowUrl;
    private string _beforeYesterdayUrl;
    private string _afterTomorowUrl;
    private HockeyGameResponse _firstResponse;
    private HockeyGameResponse _secondResponse;
    private HockeyGameResponse _thirdResponse;
    private HockeyGameResponse _fourResponse;
    private HockeyGameResponse _fiveResponse;


    private void Awake()
    {
        _todayUrl = _apiUrl + "?date=" + DateTimeController.GetDate(0).ToString("yyyy-MM-dd");
        _yesterdayUrl = _apiUrl + "?date=" + DateTimeController.GetDate(-1).ToString("yyyy-MM-dd");
        _tomorowUrl = _apiUrl + "?date=" + DateTimeController.GetDate(1).ToString("yyyy-MM-dd");
        _beforeYesterdayUrl = _apiUrl + "?date=" + DateTimeController.GetDate(-2).ToString("yyyy-MM-dd");
        _afterTomorowUrl = _apiUrl + "?date=" + DateTimeController.GetDate(2).ToString("yyyy-MM-dd");
        //LoadHandballMatches();
    }

    public void LoadHandballMatches()
    {
        StartCoroutine(GetMatchesCoroutine(_todayUrl));
        StartCoroutine(GetMatchesCoroutine(_yesterdayUrl));
        StartCoroutine(GetMatchesCoroutine(_tomorowUrl));
        StartCoroutine(GetMatchesCoroutine(_beforeYesterdayUrl));
        StartCoroutine(GetMatchesCoroutine(_afterTomorowUrl));
    }

    public void DisplayMatches(Transform container, HockeyGameResponse response, GameObject error)
    {
        if (response.response.Count <= 0)
        {
            error.SetActive(true);
        }
        else
        {
            error.SetActive(false);
        }
        foreach (var item in response.response)
        {
            MatchInfo match = Instantiate(_matchInfo, container);
            int homeGoals = 0;
            int awayGoals = 0;
            if (item.scores.home != null)
            {
                homeGoals = (int)item.scores.home;
            }
            if (item.scores.away != null)
            {
                awayGoals = (int)item.scores.away;
            }
            match.Init(item.teams.home.name, item.teams.away.name, _imageLoader, item.teams.home.logo, item.teams.away.logo, (int)item.timestamp, item.status.hockeyStatusLong, homeGoals, awayGoals);
        }
    }


    IEnumerator GetMatchesCoroutine(string url)
    {
        HockeyGameResponse footballFixture = null;
        UnityWebRequest request = UnityWebRequest.Get(url);
        request.SetRequestHeader("x-rapidapi-key", _apiKey);
        request.SetRequestHeader("x-rapidapi-host", "api-hockey.p.rapidapi.com");

        yield return request.SendWebRequest();

        if (request.result == UnityWebRequest.Result.ConnectionError || request.result == UnityWebRequest.Result.ProtocolError)
        {
            Debug.LogError(request.error);
        }
        else
        {
            string json = request.downloadHandler.text;
            HockeyGameResponse fixtureResponse = JsonConvert.DeserializeObject<HockeyGameResponse>(json);
            footballFixture = fixtureResponse;
        }
        if (url == _todayUrl)
        {
            _firstResponse = footballFixture;
            DisplayMatches(_containerOne, _firstResponse, _noMatchError[0]);
        }
        else if (url == _yesterdayUrl)
        {
            _secondResponse = footballFixture;
            DisplayMatches(_containerTwo, _secondResponse, _noMatchError[1]);
        }
        else if (url == _tomorowUrl)
        {
            _thirdResponse = footballFixture;
            DisplayMatches(_containerThree, _thirdResponse, _noMatchError[2]);
        }
        else if (url == _beforeYesterdayUrl)
        {
            _fourResponse = footballFixture;
            DisplayMatches(_containerFour, _fourResponse, _noMatchError[3]);
        }
        else if (url == _afterTomorowUrl)
        {
            _fiveResponse = footballFixture;
            DisplayMatches(_containerFive, _fiveResponse, _noMatchError[4]);
        }
    }
}

[Serializable]
public class HockeyGameResponse
{
    public string get;
    public HockeyParameters parameters;
    public List<string> errors;
    public int? results;
    public List<GameHockey> response;
}

[Serializable]
public class HockeyParameters
{
    public string date;
}

[Serializable]
public class GameHockey
{
    public int? id;
    public string date;
    public string time;
    public long timestamp;
    public string timezone;
    public string week; // ���������� object, ��� ��� �������� ����� ���� null
    public string timer; // �� �� �����
    public HockeyStatus status;
    public CountryHockey country;
    public HockeyLeague league;
    public HockeyTeams teams;
    public ScoresHockey scores;
    public HockeyPeriods periods;
    public bool? events;
}

[Serializable]
public class HockeyStatus
{
    [JsonProperty("long")]
    public string hockeyStatusLong;
    [JsonProperty("short")]
    public string hockeyStatusShort;
}

[Serializable]
public class CountryHockey
{
    public int? id;
    public string name;
    public string code;
    public string flag;
}

[Serializable]
public class HockeyLeague
{
    public int? id;
    public string name;
    public string type;
    public string logo;
    public int? season;
}

[Serializable]
public class HockeyTeams
{
    public TeamInfoHockey home;
    public TeamInfoHockey away;
}

[Serializable]
public class TeamInfoHockey
{
    public int? id;
    public string name;
    public string logo;
}

[Serializable]
public class ScoresHockey
{
    public int? home;
    public int? away;
}

[Serializable]
public class HockeyPeriods
{
    public string first;
    public string second;
    public string third;
    public string overtime;
    public string penalties;
}
