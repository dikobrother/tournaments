using Newtonsoft.Json;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class GetHandballMatches : MonoBehaviour
{
    [SerializeField] private ImageLoader _imageLoader;
    [SerializeField] private Transform _containerOne;
    [SerializeField] private Transform _containerTwo;
    [SerializeField] private Transform _containerThree;
    [SerializeField] private Transform _containerFour;
    [SerializeField] private Transform _containerFive;
    [SerializeField] private MatchInfo _matchInfo;
    [SerializeField] private List<GameObject> _noMatchError;
    private readonly string _apiKey = "c599039951fde9d17080eb48dd713268"; // �������� �� ��� ���� API
    private readonly string _apiUrl = "https://v1.handball.api-sports.io/games";
    private string _todayUrl;
    private string _yesterdayUrl;
    private string _tomorowUrl;
    private string _beforeYesterdayUrl;
    private string _afterTomorowUrl;
    private HandballScoreData _firstResponse;
    private HandballScoreData _secondResponse;
    private HandballScoreData _thirdResponse;
    private HandballScoreData _fourResponse;
    private HandballScoreData _fiveResponse;


    private void Awake()
    {
        _todayUrl = _apiUrl + "?date=" + DateTimeController.GetDate(0).ToString("yyyy-MM-dd");
        _yesterdayUrl = _apiUrl + "?date=" + DateTimeController.GetDate(-1).ToString("yyyy-MM-dd");
        _tomorowUrl = _apiUrl + "?date=" + DateTimeController.GetDate(1).ToString("yyyy-MM-dd");
        _beforeYesterdayUrl = _apiUrl + "?date=" + DateTimeController.GetDate(-2).ToString("yyyy-MM-dd");
        _afterTomorowUrl = _apiUrl + "?date=" + DateTimeController.GetDate(2).ToString("yyyy-MM-dd");
        //LoadHandballMatches();
    }

    public void LoadHandballMatches()
    {
        StartCoroutine(GetMatchesCoroutine(_todayUrl));
        StartCoroutine(GetMatchesCoroutine(_yesterdayUrl));
        StartCoroutine(GetMatchesCoroutine(_tomorowUrl));
        StartCoroutine(GetMatchesCoroutine(_beforeYesterdayUrl));
        StartCoroutine(GetMatchesCoroutine(_afterTomorowUrl));
    }

    public void DisplayMatches(Transform container, HandballScoreData response, GameObject error)
    {
        if (response.response.Length <= 0)
        {
            error.SetActive(true);
        }
        else
        {
            error.SetActive(false);
        }
        foreach (var item in response.response)
        {
            MatchInfo match = Instantiate(_matchInfo, container);
            int homeGoals = 0;
            int awayGoals = 0;
            if (item.scores.home != null)
            {
                homeGoals = (int)item.scores.home;
            }
            if (item.scores.away != null)
            {
                awayGoals = (int)item.scores.away;
            }
            match.Init(item.teams.home.name, item.teams.away.name, _imageLoader, item.teams.home.logo, item.teams.away.logo, (int)item.timestamp, item.status.longValue, homeGoals, awayGoals);
        }
    }


    IEnumerator GetMatchesCoroutine(string url)
    {
        HandballScoreData footballFixture = null;
        UnityWebRequest request = UnityWebRequest.Get(url);
        request.SetRequestHeader("x-rapidapi-key", _apiKey);
        request.SetRequestHeader("x-rapidapi-host", "v1.handball.api-sports.io");

        yield return request.SendWebRequest();

        if (request.result == UnityWebRequest.Result.ConnectionError || request.result == UnityWebRequest.Result.ProtocolError)
        {
            Debug.LogError(request.error);
        }
        else
        {
            string json = request.downloadHandler.text;
            HandballScoreData fixtureResponse = JsonConvert.DeserializeObject<HandballScoreData>(json);
            footballFixture = fixtureResponse;
        }
        if (url == _todayUrl)
        {
            _firstResponse = footballFixture;
            DisplayMatches(_containerOne, _firstResponse, _noMatchError[0]);
        }
        else if (url == _yesterdayUrl)
        {
            _secondResponse = footballFixture;
            DisplayMatches(_containerTwo, _secondResponse, _noMatchError[1]);
        }
        else if (url == _tomorowUrl)
        {
            _thirdResponse = footballFixture;
            DisplayMatches(_containerThree, _thirdResponse, _noMatchError[2]);
        }
        else if (url == _beforeYesterdayUrl)
        {
            _fourResponse = footballFixture;
            DisplayMatches(_containerFour, _fourResponse, _noMatchError[3]);
        }
        else if (url == _afterTomorowUrl)
        {
            _fiveResponse = footballFixture;
            DisplayMatches(_containerFive, _fiveResponse, _noMatchError[4]);
        }
    }
}

[System.Serializable]
public class HandballStatus
{
    [JsonProperty("long")]
    public string longValue;
    [JsonProperty("short")]
    public string shortValue;
}

[System.Serializable]
public class HandballCountry
{
    public int id;
    public string name;
    public string code;
    public string flag;
}

[System.Serializable]
public class HandballLeague
{
    public int id;
    public string name;
    public string type;
    public string logo;
    public int season;
}

[System.Serializable]
public class HandballTeams
{
    public HandballTeamInfo home;
    public HandballTeamInfo away;
}

[System.Serializable]
public class HandballTeamInfo
{
    public int id;
    public string name;
    public string logo;
}

[System.Serializable]
public class HandballScores
{
    public int? home;
    public int? away;
}

[System.Serializable]
public class HandballPeriods
{
    public HandballScoreInfo first;
    public HandballScoreInfo second;
}

[System.Serializable]
public class HandballScoreInfo
{
    public int? home;
    public int? away;
}

[System.Serializable]
public class HandballMatchInfo
{
    public int id;
    public string date;
    public string time;
    public int timestamp;
    public string timezone;
    public HandballStatus status;
    public HandballCountry country;
    public HandballLeague league;
    public HandballTeams teams;
    public HandballScores scores;
    public HandballPeriods periods;
}


[System.Serializable]
public class HandballScoreData
{
    public string get;
    public HandballParameters parameters;
    public string[] errors;
    public int results;
    public HandballMatchInfo[] response;
}

[System.Serializable]
public class HandballParameters
{
    public string date;
}



