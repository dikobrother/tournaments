using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class GetVolleyballMatches : MonoBehaviour
{
    [SerializeField] private ImageLoader _imageLoader;
    [SerializeField] private Transform _containerOne;
    [SerializeField] private Transform _containerTwo;
    [SerializeField] private Transform _containerThree;
    [SerializeField] private Transform _containerFour;
    [SerializeField] private Transform _containerFive;
    [SerializeField] private MatchInfo _matchInfo;
    [SerializeField] private List<GameObject> _noMatchError;
    private readonly string _apiKey = "c599039951fde9d17080eb48dd713268"; // �������� �� ��� ���� API
    private readonly string _apiUrl = "https://v1.volleyball.api-sports.io/games";
    private string _todayUrl;
    private string _yesterdayUrl;
    private string _tomorowUrl;
    private string _beforeYesterdayUrl;
    private string _afterTomorowUrl;
    private VolleyGames _firstResponse;
    private VolleyGames _secondResponse;
    private VolleyGames _thirdResponse;
    private VolleyGames _fourResponse;
    private VolleyGames _fiveResponse;


    private void Awake()
    {
        _todayUrl = _apiUrl + "?date=" + DateTimeController.GetDate(0).ToString("yyyy-MM-dd");
        _yesterdayUrl = _apiUrl + "?date=" + DateTimeController.GetDate(-1).ToString("yyyy-MM-dd");
        _tomorowUrl = _apiUrl + "?date=" + DateTimeController.GetDate(1).ToString("yyyy-MM-dd");
        _beforeYesterdayUrl = _apiUrl + "?date=" + DateTimeController.GetDate(-2).ToString("yyyy-MM-dd");
        _afterTomorowUrl = _apiUrl + "?date=" + DateTimeController.GetDate(2).ToString("yyyy-MM-dd");
        //LoadHandballMatches();
    }

    public void LoadHandballMatches()
    {
        StartCoroutine(GetMatchesCoroutine(_todayUrl));
        StartCoroutine(GetMatchesCoroutine(_yesterdayUrl));
        StartCoroutine(GetMatchesCoroutine(_tomorowUrl));
        StartCoroutine(GetMatchesCoroutine(_beforeYesterdayUrl));
        StartCoroutine(GetMatchesCoroutine(_afterTomorowUrl));
    }

    public void DisplayMatches(Transform container, VolleyGames response, GameObject error)
    {
        if (response.response.Count <= 0)
        {
            error.SetActive(true);
        }
        else
        {
            error.SetActive(false);
        }
        foreach (var item in response.response)
        {
            MatchInfo match = Instantiate(_matchInfo, container);
            int homeGoals = 0;
            int awayGoals = 0;
            if (item.scores.home != null)
            {
                homeGoals = (int)item.scores.home;
            }
            if (item.scores.away != null)
            {
                awayGoals = (int)item.scores.away;
            }
            match.Init(item.teams.home.name, item.teams.away.name, _imageLoader, item.teams.home.logo, item.teams.away.logo, (int)item.timestamp, item.status.longVolleyStatus, homeGoals, awayGoals);
        }
    }


    IEnumerator GetMatchesCoroutine(string url)
    {
        VolleyGames footballFixture = null;
        UnityWebRequest request = UnityWebRequest.Get(url);
        request.SetRequestHeader("x-rapidapi-key", _apiKey);
        request.SetRequestHeader("x-rapidapi-host", "api-volleyball.p.rapidapi.com");

        yield return request.SendWebRequest();

        if (request.result == UnityWebRequest.Result.ConnectionError || request.result == UnityWebRequest.Result.ProtocolError)
        {
            Debug.LogError(request.error);
        }
        else
        {
            string json = request.downloadHandler.text;
            VolleyGames fixtureResponse = JsonConvert.DeserializeObject<VolleyGames>(json);
            footballFixture = fixtureResponse;
        }
        if (url == _todayUrl)
        {
            _firstResponse = footballFixture;
            DisplayMatches(_containerOne, _firstResponse, _noMatchError[0]);
        }
        else if (url == _yesterdayUrl)
        {
            _secondResponse = footballFixture;
            DisplayMatches(_containerTwo, _secondResponse, _noMatchError[1]);
        }
        else if (url == _tomorowUrl)
        {
            _thirdResponse = footballFixture;
            DisplayMatches(_containerThree, _thirdResponse, _noMatchError[2]);
        }
        else if (url == _beforeYesterdayUrl)
        {
            _fourResponse = footballFixture;
            DisplayMatches(_containerFour, _fourResponse, _noMatchError[3]);
        }
        else if (url == _afterTomorowUrl)
        {
            _fiveResponse = footballFixture;
            DisplayMatches(_containerFive, _fiveResponse, _noMatchError[4]);
        }
    }
}

[Serializable]
public class VolleyGames
{
    public ParametersVolley parameters { get; set; }
    public List<string> errors { get; set; }
    public int results { get; set; }
    public List<ResponseVolley> response { get; set; }
}

[Serializable]
public class ParametersVolley
{
    public string date { get; set; }
}

[Serializable]
public class StatusVolley
{
    [JsonProperty("long")]
    public string longVolleyStatus { get; set; }
    [JsonProperty("short")]
    public string shortVolleyStatus { get; set; }
}

[Serializable]
public class CountryVolley
{
    public int id { get; set; }
    public string name { get; set; }
    public string code { get; set; }
    public string flag { get; set; }
}

[Serializable]
public class LeagueVolley
{
    public int id { get; set; }
    public string name { get; set; }
    public string type { get; set; }
    public string logo { get; set; }
    public int season { get; set; }
}

[Serializable]
public class TeamVolley
{
    public int id { get; set; }
    public string name { get; set; }
    public string logo { get; set; }
}

[Serializable]
public class TeamsVolley
{
    public TeamVolley home { get; set; }
    public TeamVolley away { get; set; }
}

[Serializable]
public class ScoresVolley
{
    public int? home { get; set; }
    public int? away { get; set; }
}

[Serializable]
public class PeriodVolley
{
    public int? home { get; set; }
    public int? away { get; set; }
}

[Serializable]
public class PeriodsVolley
{
    public PeriodVolley first { get; set; }
    public PeriodVolley second { get; set; }
    public PeriodVolley third { get; set; }
    public PeriodVolley fourth { get; set; }
    public PeriodVolley fifth { get; set; }
}

[Serializable]
public class ResponseVolley
{
    public int id { get; set; }
    public string date { get; set; }
    public string time { get; set; }
    public long timestamp { get; set; }
    public string timezone { get; set; }
    public string week { get; set; } 
    public StatusVolley status { get; set; }
    public CountryVolley country { get; set; }
    public LeagueVolley league { get; set; }
    public TeamsVolley teams { get; set; }
    public ScoresVolley scores { get; set; }
    public PeriodsVolley periods { get; set; }
}
