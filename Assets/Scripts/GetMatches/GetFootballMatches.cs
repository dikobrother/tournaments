using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class GetFootballMatches : MonoBehaviour
{
    [SerializeField] private ImageLoader _imageLoader;
    [SerializeField] private Transform _containerOne;
    [SerializeField] private Transform _containerTwo;
    [SerializeField] private Transform _containerThree;
    [SerializeField] private Transform _containerFour;
    [SerializeField] private Transform _containerFive;
    [SerializeField] private List<GameObject> _noMatchError;
    [SerializeField] private MatchInfo _matchInfo;
    private readonly string _apiKey = "c599039951fde9d17080eb48dd713268"; // �������� �� ��� ���� API
    private readonly string _apiUrl = "https://v3.football.api-sports.io/fixtures";
    private string _todayUrl;
    private string _yesterdayUrl;
    private string _tomorowUrl;
    private string _beforeYesterdayUrl;
    private string _afterTomorowUrl;
    private FootballFixtureResponse _firstResponse;
    private FootballFixtureResponse _secondResponse;
    private FootballFixtureResponse _thirdResponse;
    private FootballFixtureResponse _fourResponse;
    private FootballFixtureResponse _fiveResponse;


    private void Awake()
    {
        _todayUrl = _apiUrl + "?date=" + DateTimeController.GetDate(0).ToString("yyyy-MM-dd");
        _yesterdayUrl = _apiUrl + "?date=" + DateTimeController.GetDate(-1).ToString("yyyy-MM-dd");
        _tomorowUrl = _apiUrl + "?date=" + DateTimeController.GetDate(1).ToString("yyyy-MM-dd");
        _beforeYesterdayUrl = _apiUrl + "?date=" + DateTimeController.GetDate(-2).ToString("yyyy-MM-dd");
        _afterTomorowUrl = _apiUrl + "?date=" + DateTimeController.GetDate(2).ToString("yyyy-MM-dd");
        //LoadFootballMatches();
    }

    public void LoadFootballMatches()
    {
        StartCoroutine(GetMatchesCoroutine(_todayUrl));
        StartCoroutine(GetMatchesCoroutine(_yesterdayUrl));
        StartCoroutine(GetMatchesCoroutine(_tomorowUrl));
        StartCoroutine(GetMatchesCoroutine(_beforeYesterdayUrl));
        StartCoroutine(GetMatchesCoroutine(_afterTomorowUrl));
    }

    public void DisplayMatches(Transform container, FootballFixtureResponse response, GameObject error)
    {
        if (response.response.Length <= 0)
        {
            error.SetActive(true);
        }
        else
        {
            error.SetActive(false);
        }
        foreach (var item in response.response)
        {
            MatchInfo match = Instantiate(_matchInfo, container);
            int homeGoals = 0;
            int awayGoals = 0;
            if(item.goals.home != null)
            {
                homeGoals = (int)item.goals.home;
            }
            if(item.goals.away != null)
            {
                awayGoals = (int)item.goals.away;
            }
            match.Init(item.teams.home.name, item.teams.away.name, _imageLoader, item.teams.home.logo, item.teams.away.logo, (int)item.fixture.timestamp, item.fixture.status.longStr, homeGoals, awayGoals);
        }
    }


    IEnumerator GetMatchesCoroutine(string url)
    {
        FootballFixtureResponse footballFixture = null;
        UnityWebRequest request = UnityWebRequest.Get(url);
        request.SetRequestHeader("x-rapidapi-key", _apiKey);
        request.SetRequestHeader("x-rapidapi-host", "v3.football.api-sports.io");

        yield return request.SendWebRequest();

        if (request.result == UnityWebRequest.Result.ConnectionError || request.result == UnityWebRequest.Result.ProtocolError)
        {
            Debug.LogError(request.error);
        }
        else
        {
            string json = request.downloadHandler.text;
            FootballFixtureResponse fixtureResponse = JsonConvert.DeserializeObject<FootballFixtureResponse>(json);
            footballFixture = fixtureResponse;
        }
        if (url == _todayUrl)
        {
            _firstResponse = footballFixture;
            DisplayMatches(_containerOne, _firstResponse, _noMatchError[0]);
        }
        else if (url == _yesterdayUrl)
        {
            _secondResponse = footballFixture;
            DisplayMatches(_containerTwo, _secondResponse, _noMatchError[1]);
        }
        else if (url == _tomorowUrl)
        {
            _thirdResponse = footballFixture;
            DisplayMatches(_containerThree, _thirdResponse, _noMatchError[2]);
        }
        else if (url == _beforeYesterdayUrl)
        {
            _fourResponse = footballFixture;
            DisplayMatches(_containerFour, _fourResponse, _noMatchError[3]);
        }
        else if (url == _afterTomorowUrl)
        {
            _fiveResponse = footballFixture;
            DisplayMatches(_containerFive, _fiveResponse, _noMatchError[4]);
        }
    }
}

[Serializable]
public class FootballFixtureResponse
{
    public string get;
    public FootballParameters parameters;
    public string[] errors;
    public int? results;
    public FootballPaging paging;
    public FootballResponse[] response;
}

[Serializable]
public class FootballParameters
{
    public string date;
}

[Serializable]
public class FootballPaging
{
    public int? current;
    public int? total;
}

[Serializable]
public class FootballResponse
{
    public FootballFixture fixture;
    public FootballLeague league;
    public FootballTeams teams;
    public FootballGoals goals;
    public FootballScore score;
}

[Serializable]
public class FootballFixture
{
    public int? id;
    public string referee; 
    public string timezone;
    public string date;
    public int? timestamp;
    public FootballPeriods periods;
    public FootballVenue venue;
    public FootballStatus status;
}

[Serializable]
public class FootballPeriods
{
    public int? first;
    public int? second;
}

[Serializable]
public class FootballVenue
{
    public int? id;
    public string name;
    public string city;
}

[Serializable]
public class FootballStatus
{
    [JsonProperty("long")]
    public string longStr { get; set; }
    [JsonProperty("short")]
    public string shortStr { get; set; }
    public int? elapsed;
}

[Serializable]
public class FootballLeague
{
    public int? id;
    public string name;
    public string country;
    public string logo;
    public string flag;
    public int? season;
    public string round;
}

[Serializable]
public class FootballTeams
{
    public FootballTeam home;
    public FootballTeam away;
}

[Serializable]
public class FootballTeam
{
    public int? id;
    public string name;
    public string logo;
    public bool? winner;
}

[Serializable]
public class FootballGoals
{
    public int? home;
    public int? away;
}

[Serializable]
public class FootballScore
{
    public FootballHalfTime halftime;
    public FootballHalfTime fulltime;
    public FootballHalfTime extratime;
    public FootballHalfTime penalty;
}

[Serializable]
public class FootballHalfTime
{
    public int? home;
    public int? away;
}
