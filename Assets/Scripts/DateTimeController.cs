using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class DateTimeController
{
    public static DateTime GetDate(int index)
    {
        switch (index)
        {
            case 0:
                {
                    return DateTime.Today;
                }
            case 1:
                {
                    return DateTime.Today.AddDays(1);
                }
            case 2:
                {
                    return DateTime.Today.AddDays(2);
                }
            case -1:
                {
                    return DateTime.Today.AddDays(-1);
                }
            case -2:
                {
                    return DateTime.Today.AddDays(-2);
                }
        }
        return DateTime.Today;
    }

}
