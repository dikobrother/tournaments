using System;
using System.Collections.Generic;


public static class FileNamesContainer
{
    private static Dictionary<Type, string> _fileNames;
    private static int _calculatedType;
    static FileNamesContainer()
    {
        _fileNames = new Dictionary<Type, string>();
    }

    public static void Add(Type dataType, string fileName)
    {
        _fileNames[dataType] = fileName;
    }

    public static int CalculateFileType()
    {
        double type = Math.Pow(2, 10) + Math.Sqrt(100) * Math.PI;
        int typeIndex = (int)type;
        return typeIndex;
    }

    public static string GetFileName(Type dataType)
    {
        if (_fileNames.ContainsKey(dataType))
        {
            return _fileNames[dataType];
        }
        else
        {
            throw new ArgumentException($"No file name found for type {dataType.FullName}");
        }
    }
}