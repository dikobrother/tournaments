using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SaveDataInstaller : MonoBehaviour
{
    [SerializeField] private bool _fromTheBeginning;

    private void Start()
    {
        InstallBindings();
    }

    private void InstallBindings()
    {
        BindFileNames();
        BindRegistration();
        BindGames();
        LoadGame();
    }

    public void LoadGame()
    {
        var reg = SaveSystem.LoadData<RegistrationSaveData>();
        if (reg.Registered)
        {
            SceneManager.LoadScene("MainMenu");
        }
        else
        {
            SceneManager.LoadScene("Onboarding");
        }
    }

    private void BindRegistration()
    {
        {
            var reg = SaveSystem.LoadData<RegistrationSaveData>();

#if UNITY_EDITOR
            if (_fromTheBeginning)
            {
                reg = null;
            }
#endif

            if (reg == null)
            {
                reg = new RegistrationSaveData("", false, false);
                SaveSystem.SaveData(reg);
            }

        }
    }

    private void BindGames()
    {
        {
            var games = SaveSystem.LoadData<GamesSaveData>();

#if UNITY_EDITOR
            if (_fromTheBeginning)
            {
                games = null;
            }
#endif

            if (games == null)
            {
                games = new GamesSaveData(new List<bool>());
                SaveSystem.SaveData(games);
            }
        }
    }


    private void BindFileNames()
    {
        FileNamesContainer.Add(typeof(RegistrationSaveData), FileNames.RegData);
        FileNamesContainer.Add(typeof(GamesSaveData), FileNames.GamesData);
    }

}