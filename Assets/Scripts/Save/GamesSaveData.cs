using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class GamesSaveData : SaveData
{
    public List<bool> Games { get; set; }

    public GamesSaveData(List<bool> games)
    {
        Games = games;
    }
}
