using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class TournamentTeamStats : MonoBehaviour
{
    [SerializeField] private TMP_Text _teamName;
    [SerializeField] private TMP_Text _gamesText;
    [SerializeField] private TMP_Text _winText;
    [SerializeField] private TMP_Text _drawText;
    [SerializeField] private TMP_Text _loseText;
    [SerializeField] private TMP_Text _scoreText;

    public void Init(string name, int games, int win, int draw, int lose, int score)
    {
        _teamName.text = name;
        _gamesText.text = games.ToString();
        _winText.text = win.ToString();
        _drawText.text = draw.ToString();
        _loseText.text = lose.ToString();
        _scoreText.text = score.ToString();
    }
}
