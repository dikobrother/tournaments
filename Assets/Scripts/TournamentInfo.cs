using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class TournamentInfo : MonoBehaviour
{
    [SerializeField] private TMP_Text _leagueName;
    [SerializeField] private TMP_Text _typeName;
    [SerializeField] private TMP_Text _season;
    [SerializeField] private Button _showButton;
    private int _leagueId;
    private int _leagueSeason;
    private string _url;
    private string _header;
    private string _type;
    private TournamentInfoShower _tournamentInfoShower;

    public void Init(string name, string type, string seasonStart, string seasonEnd, int leagueId, int leagueSeason, TournamentInfoShower tournamentInfoShower, string url, string header)
    {
        _leagueName.text = name;
        _typeName.text = type;
        _season.text = seasonStart + "/\n" + seasonEnd;
        _leagueId = leagueId;
        _leagueSeason = leagueSeason;
        _tournamentInfoShower = tournamentInfoShower;
        _url = url;
        _header = header;
        _type = type;
        _showButton.onClick.RemoveAllListeners();
        _showButton.onClick.AddListener(() => OnShowInfoButtonClick(seasonStart, seasonEnd, name));
    }

    private void OnShowInfoButtonClick(string start, string end, string name)
    {
        _tournamentInfoShower.ShowInfo(_leagueId, _leagueSeason, _url, _header, _type, name, start + " | " + end);
    }
}
