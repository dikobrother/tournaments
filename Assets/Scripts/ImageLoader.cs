using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class ImageLoader : MonoBehaviour
{
    private Queue<ImageData> imageQueue = new Queue<ImageData>();

    private bool isLoading = false;

    public void AddImageToQueue(string imageUrl, Image image)
    {
        ImageData img = new ImageData();
        img.Url = imageUrl;
        img.Image = image;
        if (imageUrl != "")
        {
            imageQueue.Enqueue(img);
        }
        if (!isLoading)
        {
            StartCoroutine(LoadImagesFromQueue());
        }
    }

    private IEnumerator LoadImagesFromQueue()
    {
        isLoading = true;

        while (imageQueue.Count > 0)
        {
            ImageData imageData = imageQueue.Dequeue();
            UnityWebRequest request = UnityWebRequestTexture.GetTexture(imageData.Url);
            yield return request.SendWebRequest();

            if (request.result == UnityWebRequest.Result.Success)
            {
                Texture2D texture = ((DownloadHandlerTexture)request.downloadHandler).texture;
                Sprite sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2());
                imageData.Image.sprite = sprite;
            }
            //yield return new WaitForSeconds(1f);
        }

        isLoading = false;
    }
}

public class ImageData
{
    public string Url;
    public Image Image;
}