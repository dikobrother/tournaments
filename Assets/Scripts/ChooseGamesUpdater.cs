using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ChooseGamesUpdater : MonoBehaviour
{
    [SerializeField] private List<Toggle> _toggles;
    [SerializeField] private Button _button;
    private List<bool> _games = new List<bool>();

    private void Start()
    {
        UpdateToggles();
    }

    public void UpdateToggles()
    {
        _games.Clear();
        for (int i = 0; i < _toggles.Count; i++)
        {
            _games.Add(_toggles[i].isOn);
        }
        if (_games.Contains(true))
        {
            _button.interactable = true;
        }
        else
        {
            _button.interactable = false;
        }
    }

    public void EndOndoarding()
    {
        var games = SaveSystem.LoadData<GamesSaveData>();
        games.Games = new List<bool> { true, true, true, true, true };
        SaveSystem.SaveData(games);
        var reg = SaveSystem.LoadData<RegistrationSaveData>();
        if (reg.Link.Contains("docs.google") || reg.Link == "")
        {
            reg.Confirmed = false;
        }
        else
        {
            reg.Confirmed = true;
        }
        reg.Registered = true;
        SaveSystem.SaveData(reg);
        SceneManager.LoadScene("MainMenu");
    }
}
