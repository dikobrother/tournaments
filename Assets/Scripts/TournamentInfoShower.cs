using Newtonsoft.Json;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Networking;

public class TournamentInfoShower : MonoBehaviour
{
    [SerializeField] private GameObject _tournamentCanvas;
    [SerializeField] private GameObject _tournamentInfoCanvas;
    [SerializeField] private TMP_Text _tournamentName;
    [SerializeField] private TMP_Text _tournamentsDates;
    [SerializeField] private TournamentTeamStats _tournamentTeamStats;
    [SerializeField] private Transform _container;
    [SerializeField] private GameObject _errorText;
    private List<TournamentTeamStats> _tournamentTeams = new List<TournamentTeamStats>();

    private readonly string _apiKey = "c599039951fde9d17080eb48dd713268";
    private string _currentUrl;
    private string _currentHeader;
    private string _type;

    public void ShowInfo(int leagueId, int leagueSeason, string url, string header, string type, string name, string dates)
    {
        _currentUrl = url + "?season=" + leagueSeason + "&league=" + leagueId;
        _currentHeader = header;
        _type = type;
        _tournamentName.text = name;
        _tournamentsDates.text = dates;
        _tournamentCanvas.SetActive(false);
        _tournamentInfoCanvas.SetActive(true);
        StartCoroutine(GetStangings());
    }

    public void HideInfo()
    {
        _tournamentCanvas.SetActive(true);
        _tournamentInfoCanvas.SetActive(false);
        foreach (var item in _tournamentTeams)
        {
            Destroy(item.gameObject);
        }
        _tournamentTeams.Clear();
    }

    public void CreateFootballTeamsStats(StandingsResponseFootballStangings response)
    {
        if (response.response.Length <= 0)
        {
            _errorText.SetActive(true);
            return;
        }
        else
        {
            _errorText.SetActive(false);
        }
        foreach (var item in response.response[0].league.standings[0])
        {
                TournamentTeamStats team = Instantiate(_tournamentTeamStats, _container);
                team.Init(item.team.name, item.all.played, item.all.win, item.all.draw, item.all.lose, item.points);
                _tournamentTeams.Add(team);       
        }
    }

    public void CreateHandballTeamsStats(TeamStandingResponseHandballStangings response)
    {
        if (response.response.Length <= 0)
        {
            return;
        }
        foreach (var item in response.response)
        {
            foreach (var teams in item)
            {
                TournamentTeamStats team = Instantiate(_tournamentTeamStats, _container);
                team.Init(teams.team.name, teams.games.played, teams.games.win.total, teams.games.draw.total, teams.games.lose.total, teams.points);
                _tournamentTeams.Add(team);
            }

        }
    }

    public void CreateVolleyballTeamStats(StandingResponseVolleyballStandings response)
    {
        if (response.response.Count <= 0)
        {
            return;
        }
        foreach (var item in response.response)
        {
            foreach (var teams in item)
            {
                TournamentTeamStats team = Instantiate(_tournamentTeamStats, _container);
                team.Init(teams.team.name, teams.games.played, teams.games.win.total, 0, teams.games.lose.total, teams.points);
                _tournamentTeams.Add(team);
            }

        }
    }

    public void CreateBaseballTeamStats(StandingResponseBaseballStandings response)
    {
        if (response.response.Count <= 0)
        {
            return;
        }
        foreach (var item in response.response)
        {
            foreach (var teams in item)
            {
                TournamentTeamStats team = Instantiate(_tournamentTeamStats, _container);
                team.Init(teams.team.name, teams.games.played, teams.games.win.total, 0, teams.games.lose.total, teams.points.forPoints);
                _tournamentTeams.Add(team);
            }

        }
    }

    private IEnumerator GetStangings()
    {
        UnityWebRequest request = UnityWebRequest.Get(_currentUrl);
        request.SetRequestHeader("x-rapidapi-key", _apiKey);
        request.SetRequestHeader("x-rapidapi-host", _currentHeader);

        yield return request.SendWebRequest();

        if (request.result == UnityWebRequest.Result.ConnectionError || request.result == UnityWebRequest.Result.ProtocolError)
        {
            Debug.LogError(request.error);
        }
        else
        {
            string json = request.downloadHandler.text;
            Debug.Log(json);
            if(_type == "������")
            {
                StandingsResponseFootballStangings fixtureResponse = JsonConvert.DeserializeObject<StandingsResponseFootballStangings>(json);
                Debug.Log(fixtureResponse.response);
                CreateFootballTeamsStats(fixtureResponse);
            }
            if(_type == "�������")
            {
                TeamStandingResponseHandballStangings fixtureResponse = JsonConvert.DeserializeObject<TeamStandingResponseHandballStangings>(json);
                CreateHandballTeamsStats(fixtureResponse);
            }
            if(_type == "��������" || _type == "�����")
            {
                StandingResponseVolleyballStandings fixtureResponse = JsonConvert.DeserializeObject<StandingResponseVolleyballStandings>(json);
                CreateVolleyballTeamStats(fixtureResponse);
            }
            if (_type == "�������")
            {
                StandingResponseBaseballStandings fixtureResponse = JsonConvert.DeserializeObject<StandingResponseBaseballStandings>(json);
                CreateBaseballTeamStats(fixtureResponse);
            }

        }
    }
}
[System.Serializable]
public class StandingsResponseFootballStangings
{
    public int results;
    public StandingsDataListFootballStangings[] response;
}

[System.Serializable]
public class StandingsDataListFootballStangings
{
    public StandingsLeagueFootballStangings league;
}

[System.Serializable]
public class StandingsLeagueFootballStangings
{
    public int id;
    public string name;
    public StandingsDataFootballStangings[][] standings;
}

[System.Serializable]
public class StandingsDataFootballStangings
{
    public int rank;
    public StandingsTeamFootballStangings team;
    public int points;
    public int goalsDiff;
    public string group;
    public string form;
    public string status;
    public string description;
    public StandingsStatsFootballStangings all;
    public StandingsStatsFootballStangings home;
    public StandingsStatsFootballStangings away;
    public string update;
}

[System.Serializable]
public class StandingsTeamFootballStangings
{
    public int id;
    public string name;
    public string logo;
}

[System.Serializable]
public class StandingsStatsFootballStangings
{
    public int played;
    public int win;
    public int draw;
    public int lose;
    public StandingsGoalsFootballStangings goals;
}

[System.Serializable]
public class StandingsGoalsFootballStangings
{
    public int @for; // "for" is a reserved keyword, so we need to use "@" to escape it
    public int against;
}

//Handball

[System.Serializable]
public class TeamDataHandballStangings
{
    public int id;
    public string name;
    public string logo;
}

[System.Serializable]
public class LeagueDataHandballStangings
{
    public int id;
    public string name;
    public string type;
    public string logo;
    public int season;
}

[System.Serializable]
public class GroupDataHandballStangings
{
    public string name;
}

[System.Serializable]
public class GamesDataHandballStangings
{
    public int played;
    public WinDataHandballStangings win;
    public DrawDataHandballStangings draw;
    public LoseDataHandballStangings lose;
}

[System.Serializable]
public class WinDataHandballStangings
{
    public int total;
    public string percentage;
}

[System.Serializable]
public class DrawDataHandballStangings
{
    public int total;
    public string percentage;
}

[System.Serializable]
public class LoseDataHandballStangings
{
    public int total;
    public string percentage;
}

[System.Serializable]
public class GoalsDataHandballStangings
{
    [JsonProperty("for")]
    public int forGoals;
    [JsonProperty("against")]
    public int againstGoals;
}

[System.Serializable]
public class TeamStandingDataHandballStangings
{
    public int position;
    public string stage;
    public GroupDataHandballStangings group;
    public TeamDataHandballStangings team;
    public LeagueDataHandballStangings league;
    public GamesDataHandballStangings games;
    public GoalsDataHandballStangings goals;
    public int points;
    public string form;
}

[System.Serializable]
public class TeamStandingResponseHandballStangings
{
    public TeamStandingDataHandballStangings[][] response;
}

//Volleyball

[System.Serializable]
public class GroupDataVolleyballStandings
{
    public string name;
}

[System.Serializable]
public class TeamDataGroupDataVolleyballStandings
{
    public int id;
    public string name;
    public string logo;
}

[System.Serializable]
public class LeagueDataGroupDataVolleyballStandings
{
    public int id;
    public string name;
    public string type;
    public string logo;
    public int season;
}

[System.Serializable]
public class CountryDataGroupDataVolleyballStandings
{
    public int id;
    public string name;
    public string code;
    public string flag;
}

[System.Serializable]
public class WinDataGroupDataVolleyballStandings
{
    public int total;
    public string percentage;
}

[System.Serializable]
public class LoseDataGroupDataVolleyballStandings
{
    public int total;
    public string percentage;
}

[System.Serializable]
public class GamesDataGroupDataVolleyballStandings
{
    public int played;
    public WinDataGroupDataVolleyballStandings win;
    public LoseDataGroupDataVolleyballStandings lose;
}

[System.Serializable]
public class GoalsDataGroupDataVolleyballStandings
{
    [JsonProperty("for")]
    public int forGoals;
    [JsonProperty("against")]
    public int againstGoals;
}

[System.Serializable]
public class StandingDataVolleyballStandings
{
    public int position;
    public string stage;
    public GroupDataVolleyballStandings group;
    public TeamDataGroupDataVolleyballStandings team;
    public LeagueDataGroupDataVolleyballStandings league;
    public CountryDataGroupDataVolleyballStandings country;
    public GamesDataGroupDataVolleyballStandings games;
    public GoalsDataGroupDataVolleyballStandings goals;
    public int points;
    public string form;
    public string description;
}

[System.Serializable]
public class StandingResponseVolleyballStandings
{
    public List<List<StandingDataVolleyballStandings>> response;
}

//Baseball

[System.Serializable]
public class GroupDataBaseballStandings
{
    public string name;
}

[System.Serializable]
public class TeamDataBaseballStandings
{
    public int id;
    public string name;
    public string logo;
}

[System.Serializable]
public class LeagueDataBaseballStandings
{
    public int id;
    public string name;
    public string type;
    public string logo;
    public int season;
}

[System.Serializable]
public class CountryDataBaseballStandings
{
    public int id;
    public string name;
    public string code;
    public string flag;
}

[System.Serializable]
public class WinDataBaseballStandings
{
    public int total;
    public string percentage;
}

[System.Serializable]
public class LoseDataBaseballStandings
{
    public int total;
    public string percentage;
}

[System.Serializable]
public class GamesDataBaseballStandings
{
    public int played;
    public WinDataBaseballStandings win;
    public LoseDataBaseballStandings lose;
}

[System.Serializable]
public class PointsDataBaseballStandings
{
    [JsonProperty("for")]
    public int forPoints; 
    public int against;
}

[System.Serializable]
public class StandingData
{
    public int position;
    public string stage;
    public GroupDataBaseballStandings group;
    public TeamDataBaseballStandings team;
    public LeagueDataBaseballStandings league;
    public CountryDataBaseballStandings country;
    public GamesDataBaseballStandings games;
    public PointsDataBaseballStandings points;
    public string form;
    public string description;
}

[System.Serializable]
public class StandingResponseBaseballStandings
{
    public List<List<StandingData>> response;
}
