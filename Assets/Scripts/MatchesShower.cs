using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class MatchesShower : MonoBehaviour
{
    [SerializeField] private List<TMP_Text> _dates;
    [SerializeField] private List<TMP_Text> _dayOfWeeks;
    [SerializeField] private List<Image> _dateButtons;
    [SerializeField] private Sprite _selectedDay;
    [SerializeField] private Sprite _unselectedDay;
    [SerializeField] private List<GameObject> _panels;

    private void Start()
    {
        for (int i = 0; i < _dates.Count; i++)
        {
            switch (i)
            {
                case 0:
                    {
                        _dates[i].text = DateTimeController.GetDate(-2).Day.ToString();
                        _dayOfWeeks[i].text = DateTimeController.GetDate(-2).ToString("ddd",
                        new CultureInfo("ru-RU"));
                        break;
                    }
                case 1:
                    {
                        _dates[i].text = DateTimeController.GetDate(-1).Day.ToString();
                        _dayOfWeeks[i].text = DateTimeController.GetDate(-1).ToString("ddd",
                        new CultureInfo("ru-RU"));
                        break;
                    }
                case 2:
                    {
                        _dates[i].text = DateTimeController.GetDate(0).Day.ToString();
                        _dayOfWeeks[i].text = DateTimeController.GetDate(0).ToString("ddd",
                        new CultureInfo("ru-RU"));
                        break;
                    }
                case 3:
                    {
                        _dates[i].text = DateTimeController.GetDate(1).Day.ToString();
                        _dayOfWeeks[i].text = DateTimeController.GetDate(1).ToString("ddd",
                        new CultureInfo("ru-RU"));
                        break;
                    }
                case 4:
                    {
                        _dates[i].text = DateTimeController.GetDate(2).Day.ToString();
                        _dayOfWeeks[i].text = DateTimeController.GetDate(2).ToString("ddd",
                        new CultureInfo("ru-RU"));
                        break;
                    }
            }
        }
        ShowDayMatches(2);
    }

    public void ShowDayMatches(int index)
    {
        foreach (var item in _panels)
        {
            item.SetActive(false);
        }
        foreach (var item in _dateButtons)
        {
            item.sprite = _unselectedDay;
        }
        _dateButtons[index].sprite = _selectedDay;
        _panels[index].SetActive(true);
    }
}
