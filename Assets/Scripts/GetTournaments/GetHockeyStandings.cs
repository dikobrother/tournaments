using Newtonsoft.Json;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class GetHockeyStandings : MonoBehaviour
{
    [SerializeField] private Transform _container;
    [SerializeField] private TournamentInfoShower _tournamentInfoShower;
    [SerializeField] private TournamentInfo _tournamentInfo;
    private readonly string _apiKey = "c599039951fde9d17080eb48dd713268"; // �������� �� ��� ���� API
    private readonly string _apiLeaguesUrl = "https://v1.hockey.api-sports.io/leagues";
    private readonly string _apiStandingsUrl = "https://v1.hockey.api-sports.io/standings";
    private readonly string _apiHeader = "v1.hockey.api-sports.io";
    private string _oneLeagueUrl;
    private string _twoLeagueUrl;
    private string _threeLeagueUrl;
    private LeagueResponseHandball _firstLeaguesResponse;
    private LeagueResponseHandball _secondLeaguesResponse;
    private LeagueResponseHandball _thirdLeaguesResponse;
    private List<TournamentInfo> _tournaments2024 = new List<TournamentInfo>();
    private List<TournamentInfo> _tournaments2023 = new List<TournamentInfo>();
    private List<TournamentInfo> _tournaments2022 = new List<TournamentInfo>();


    private void Awake()
    {
        _oneLeagueUrl = _apiLeaguesUrl + "?season=" + 2024;
        _twoLeagueUrl = _apiLeaguesUrl + "?season=" + 2023;
        _threeLeagueUrl = _apiLeaguesUrl + "?season=" + 2022;
        //LoadVolleyballLeagues();
    }
    public void HideAllMatches()
    {
        foreach (var item in _tournaments2024)
        {
            item.gameObject.SetActive(false);
        }
        foreach (var item in _tournaments2023)
        {
            item.gameObject.SetActive(false);
        }
        foreach (var item in _tournaments2022)
        {
            item.gameObject.SetActive(false);
        }
    }

    public void ShowMatchesBySeason(int season)
    {
        if (season == 2024)
        {
            foreach (var item in _tournaments2024)
            {
                item.gameObject.SetActive(true);
            }
        }
        if (season == 2023)
        {
            foreach (var item in _tournaments2023)
            {
                item.gameObject.SetActive(true);
            }
        }
        if (season == 2022)
        {
            foreach (var item in _tournaments2022)
            {
                item.gameObject.SetActive(true);
            }
        }
    }

    public void LoadVolleyballLeagues()
    {
        StartCoroutine(LoadAllLeagues());

    }

    IEnumerator LoadAllLeagues()
    {
        StartCoroutine(GetLeaguesCoroutine(_oneLeagueUrl));
        yield return new WaitForSeconds(0f);
        StartCoroutine(GetLeaguesCoroutine(_twoLeagueUrl));
        yield return new WaitForSeconds(0f);
        StartCoroutine(GetLeaguesCoroutine(_threeLeagueUrl));
    }

    public void DisplayLeague(LeagueResponseHandball leagues)
    {
        foreach (var item in leagues.response)
        {
            TournamentInfo match = Instantiate(_tournamentInfo, _container);
            match.Init(item.name, "�����", item.seasons[0].start, item.seasons[0].end, item.id, item.seasons[0].season, _tournamentInfoShower, _apiStandingsUrl, _apiHeader);
            if (item.seasons[0].season == 2024)
            {
                _tournaments2024.Add(match);
            }
            if (item.seasons[0].season == 2023)
            {
                _tournaments2023.Add(match);
            }
            if (item.seasons[0].season == 2022)
            {
                _tournaments2022.Add(match);
            }
        }
    }


    IEnumerator GetLeaguesCoroutine(string url)
    {
        LeagueResponseHandball footballFixture = null;
        UnityWebRequest request = UnityWebRequest.Get(url);
        request.SetRequestHeader("x-rapidapi-key", _apiKey);
        request.SetRequestHeader("x-rapidapi-host", _apiHeader);

        yield return request.SendWebRequest();

        if (request.result == UnityWebRequest.Result.ConnectionError || request.result == UnityWebRequest.Result.ProtocolError)
        {
            Debug.LogError(request.error);
        }
        else
        {
            string json = request.downloadHandler.text;
            Debug.Log(json);
            LeagueResponseHandball fixtureResponse = JsonConvert.DeserializeObject<LeagueResponseHandball>(json);
            footballFixture = fixtureResponse;
        }
        if (url == _oneLeagueUrl)
        {
            _firstLeaguesResponse = footballFixture;
            DisplayLeague(_firstLeaguesResponse);
        }
        else if (url == _twoLeagueUrl)
        {
            _secondLeaguesResponse = footballFixture;
            DisplayLeague(_secondLeaguesResponse);
        }
        else if (url == _threeLeagueUrl)
        {
            _thirdLeaguesResponse = footballFixture;
            DisplayLeague(_thirdLeaguesResponse);
        }
    }
}
