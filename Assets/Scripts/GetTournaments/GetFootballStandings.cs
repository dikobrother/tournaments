using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class GetFootballStandings : MonoBehaviour
{
    [SerializeField] private Transform _container;
    [SerializeField] private TournamentInfoShower _tournamentInfoShower;
    [SerializeField] private TournamentInfo _tournamentInfo;
    private readonly string _apiKey = "c599039951fde9d17080eb48dd713268"; // �������� �� ��� ���� API
    private readonly string _apiLeaguesUrl = "https://v3.football.api-sports.io/leagues";
    private readonly string _apiStandingsUrl = "https://v3.football.api-sports.io/standings";
    private readonly string _apiHeader = "v3.football.api-sports.io";
    private string _oneLeagueUrl;
    private string _twoLeagueUrl;
    private string _threeLeagueUrl;
    private LeaguesDataFootball _firstLeaguesResponse;
    private LeaguesDataFootball _secondLeaguesResponse;
    private LeaguesDataFootball _thirdLeaguesResponse;
    private List<TournamentInfo> _tournaments2024 = new List<TournamentInfo>();
    private List<TournamentInfo> _tournaments2023 = new List<TournamentInfo>();
    private List<TournamentInfo> _tournaments2022 = new List<TournamentInfo>();


    private void Awake()
    {
        _oneLeagueUrl = _apiLeaguesUrl + "?season=" + 2024 + "&current=true";
        _twoLeagueUrl = _apiLeaguesUrl + "?season=" + 2023 + "&current=true";
        _threeLeagueUrl = _apiLeaguesUrl + "?season=" + 2022 + "&current=true";
        //LoadFootballLeagues();
    }

    public void HideAllMatches()
    {
        foreach (var item in _tournaments2024)
        {
            item.gameObject.SetActive(false);
        }
        foreach (var item in _tournaments2023)
        {
            item.gameObject.SetActive(false);
        }
        foreach (var item in _tournaments2022)
        {
            item.gameObject.SetActive(false);
        }
    }

    public void ShowMatchesBySeason(int season)
    {
        if (season == 2024)
        {
            foreach (var item in _tournaments2024)
            {
                item.gameObject.SetActive(true);
            }
        }
        if (season == 2023)
        {
            foreach (var item in _tournaments2023)
            {
                item.gameObject.SetActive(true);
            }
        }
        if (season == 2022)
        {
            foreach (var item in _tournaments2022)
            {
                item.gameObject.SetActive(true);
            }
        }
    }
    public void LoadFootballLeagues()
    {
        StartCoroutine(LoadAllLeagues());
        
    }

    IEnumerator LoadAllLeagues()
    {
        StartCoroutine(GetLeaguesCoroutine(_oneLeagueUrl));
        yield return new WaitForSeconds(0f);
        StartCoroutine(GetLeaguesCoroutine(_twoLeagueUrl));
        yield return new WaitForSeconds(0f);
        StartCoroutine(GetLeaguesCoroutine(_threeLeagueUrl));
    }

    public void DisplayLeague(LeaguesDataFootball leagues)
    {
        foreach (var item in leagues.response)
        {
            TournamentInfo match = Instantiate(_tournamentInfo, _container);
            match.Init(item.league.name, "������", item.seasons[0].start, item.seasons[0].end, item.league.id, item.seasons[0].year, _tournamentInfoShower, _apiStandingsUrl, _apiHeader);
            if (item.seasons[0].year == 2024)
            {
                _tournaments2024.Add(match);
            }
            if (item.seasons[0].year == 2023)
            {
                _tournaments2023.Add(match);
            }
            if (item.seasons[0].year == 2022)
            {
                _tournaments2022.Add(match);
            }
        }
    }


    IEnumerator GetLeaguesCoroutine(string url)
    {
        LeaguesDataFootball footballFixture = null;
        UnityWebRequest request = UnityWebRequest.Get(url);
        request.SetRequestHeader("x-rapidapi-key", _apiKey);
        request.SetRequestHeader("x-rapidapi-host", _apiHeader);

        yield return request.SendWebRequest();

        if (request.result == UnityWebRequest.Result.ConnectionError || request.result == UnityWebRequest.Result.ProtocolError)
        {
            Debug.LogError(request.error);
        }
        else
        {
            string json = request.downloadHandler.text;
            Debug.Log(json);
            LeaguesDataFootball fixtureResponse = JsonConvert.DeserializeObject<LeaguesDataFootball>(json);
            footballFixture = fixtureResponse;
        }
        if (url == _oneLeagueUrl)
        {
            _firstLeaguesResponse = footballFixture;
            DisplayLeague(_firstLeaguesResponse);
        }
        else if (url == _twoLeagueUrl)
        {
            _secondLeaguesResponse = footballFixture;
            DisplayLeague(_secondLeaguesResponse);
        }
        else if (url == _threeLeagueUrl)
        {
            _thirdLeaguesResponse = footballFixture;
            DisplayLeague(_thirdLeaguesResponse);
        }
    }
}

[Serializable]
public class LeagueFootball
{
    public int id;
    public string name;
    public string type;
    public string logo;
}

[Serializable]
public class CountryFootball
{
    public string name;
    public string code;
    public string flag;
}

[Serializable]
public class SeasonFootball
{
    public int year;
    public string start;
    public string end;
    public bool current;
    public CoverageFootball coverage;
}

[Serializable]
public class CoverageFootball
{
    public FixturesFootball fixtures;
    public bool standings;
    public bool players;
    public bool top_scorers;
    public bool top_assists;
    public bool top_cards;
    public bool injuries;
    public bool predictions;
    public bool odds;
}

[Serializable]
public class FixturesFootball
{
    public bool events;
    public bool lineups;
    public bool statistics_fixtures;
    public bool statistics_players;
}

[Serializable]
public class LeagueInfoFootball
{
    public LeagueFootball league;
    public CountryFootball country;
    public SeasonFootball[] seasons;
}

[Serializable]
public class PagingFootball
{
    public int current;
    public int total;
}

[Serializable]
public class LeaguesDataFootball
{
    public string get;
    public ParametersFootball parameters;
    public List<string> errors;
    public int results;
    public PagingFootball paging;
    public List<LeagueInfoFootball> response;
}


[Serializable]
public class ParametersFootball
{
    public string season;
}
