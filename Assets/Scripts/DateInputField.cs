using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Linq;

public class DateInputField : MonoBehaviour
{
    [SerializeField] private GameObject _ageCanvas;
    [SerializeField] private GameObject _gamesCanvas;
    [SerializeField] private TMP_InputField _dateInputField;
    [SerializeField] private Image _inputBackground;
    [SerializeField] private Sprite _firstInputSprite;
    [SerializeField] private Sprite _secondInputSprite;
    [SerializeField] private Sprite _thirdInputSprite;
    [SerializeField] private TMP_Text _errorMessageText;
    [SerializeField] private Button _nextButton;

    void Start()
    {
        _dateInputField.onEndEdit.AddListener(delegate { ValidateAndFormatDate(_dateInputField); });
        _dateInputField.onValueChanged.AddListener(OnInputFieldValueChanged);
    }

    private void OnInputFieldValueChanged(string value)
    {
        string digitsOnly = new string(value.Where(char.IsDigit).ToArray());

        if (digitsOnly.Length >= 2)
            digitsOnly = digitsOnly.Insert(2, ".");
        if (digitsOnly.Length >= 5)
            digitsOnly = digitsOnly.Insert(5, ".");

        _dateInputField.text = digitsOnly;
    }

    private void ValidateAndFormatDate(TMP_InputField inputField)
    {
        DateTime date;
        if (DateTime.TryParse(inputField.text, out date))
        {
            inputField.text = date.ToString("d");
            if(DateTime.Now.Year - date.Year >= 18)
            {
                _errorMessageText.gameObject.SetActive(false);
                _nextButton.interactable = true;
                _nextButton.gameObject.SetActive(true);
                _inputBackground.sprite = _secondInputSprite;
            }
            else
            {
                _errorMessageText.gameObject.SetActive(true);
                _errorMessageText.text = "��������, ��� ��� ��� 18 ���. ���� � ������� ��������.";
                _nextButton.gameObject.SetActive(false);
                _inputBackground.sprite = _thirdInputSprite;
            }
        }
        else
        {
            _errorMessageText.gameObject.SetActive(true);
            _errorMessageText.text = "������� ������� ���� ��������.\n ���������, ���� ���� ���� � ������� ��.��.����";
            _nextButton.interactable = false;
            _nextButton.gameObject.SetActive(false);
            inputField.text = "";
            _inputBackground.sprite = _firstInputSprite;
        }
    }

    public void ConfirmAge()
    {
        _ageCanvas.SetActive(false);
        _gamesCanvas.SetActive(true);
    }

}
