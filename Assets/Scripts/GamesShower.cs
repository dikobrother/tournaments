using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GamesShower : MonoBehaviour
{
    [SerializeField] private GameObject _menuCanvas;
    [SerializeField] private List<GameObject> _mathesCanvases;
    [SerializeField] private List<string> _gameNames;
    [SerializeField] private TMP_Text _gameName;
    [SerializeField] private Button _nextGame;
    [SerializeField] private Button _previousGame;
    [SerializeField] private GetFootballMatches _footballMatches;
    [SerializeField] private GetHandballMatches _handballMatches;
    [SerializeField] private GetVolleyballMatches _volleyballMatches;
    [SerializeField] private GetHockeyMatches _hockeyMatches;
    [SerializeField] private GetBaseballMatches _baseballMatches;
    [SerializeField] private GameObject _navogatorCanvas;
    private int _canvasIndex = 0;

    private void Start()
    {
        var games = SaveSystem.LoadData<GamesSaveData>();
        List<GameObject> canvases = new List<GameObject>();
        List<string> names = new List<string>();
        for (int i = 0; i < games.Games.Count; i++)
        {
            if(games.Games[i] == true)
            {
                canvases.Add(_mathesCanvases[i]);
                names.Add(_gameNames[i]);
                LoadMatches(i);
            }
            Debug.Log(i);
        }
        _mathesCanvases.Clear();
        _gameNames.Clear();
        _mathesCanvases = canvases;
        _gameNames = names;
        _nextGame.onClick.AddListener(OnNextButtonClick);
        _previousGame.onClick.AddListener(OnPreviousButtonClick);
    }

    public void LoadMatches(int index)
    {
        switch (index)
        {
            case 0:
                {
                    _footballMatches.LoadFootballMatches();
                    break;
                }
            case 1:
                {
                    _handballMatches.LoadHandballMatches();
                    break;
                }
            case 2:
                {
                    _volleyballMatches.LoadHandballMatches();
                    break;
                }
            case 3:
                {
                    _hockeyMatches.LoadHandballMatches();
                    break;
                }
            case 4:
                {
                    _baseballMatches.LoadHandballMatches();
                    break;
                }
        }
    }

    public void OpenGames()
    {
        _menuCanvas.SetActive(false);
        _mathesCanvases[_canvasIndex].SetActive(true);
        _gameName.text = _gameNames[_canvasIndex];
        _navogatorCanvas.SetActive(true);
    }

    public void CloseGames()
    {
        _menuCanvas.SetActive(true);
        _mathesCanvases[_canvasIndex].SetActive(false);
        _navogatorCanvas.SetActive(false);
    }

    public void OnNextButtonClick()
    {
        if(_canvasIndex >= _mathesCanvases.Count - 1)
        {
            _canvasIndex = 0;
        }
        else
        {
            _canvasIndex++;
        }
        foreach (var item in _mathesCanvases)
        {
            item.SetActive(false);
        }
        _mathesCanvases[_canvasIndex].SetActive(true);
        _gameName.text = _gameNames[_canvasIndex];
    }

    public void OnPreviousButtonClick()
    {
        if (_canvasIndex <= 0)
        {
            _canvasIndex = _mathesCanvases.Count - 1;
        }
        else
        {
            _canvasIndex--;
        }
        foreach (var item in _mathesCanvases)
        {
            item.SetActive(false);
        }
        _mathesCanvases[_canvasIndex].SetActive(true);
        _gameName.text = _gameNames[_canvasIndex];
    }
}
